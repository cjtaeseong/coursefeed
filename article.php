<?php

require_once("coursefeed.php");
require_once("template/article.php");
require_once("template/article_list.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

function validMonth($month) {
    return 1 <= $month && $month <= 12;
}

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    startblock('content');
    if (http\has_parameter($_GET, "id")) {
        $article = $coursefeed->getArticle($_GET["id"]);
        if ($article["latest_revision"]) {
            template\article\renderEntry($article);
        } else {
            header('HTTP/1.1 404 Not Found');
            template\article\renderNotFound();
        }
    } else {
        $articles = $coursefeed->getArticleList(http\maybe_get_parameter($_GET, "course_id"));

        template\article\renderListHeader();

        if (http\has_parameter($_GET, "month") && validMonth($_GET["month"])) {
            template\article\renderCalendar($articles, $_GET["month"]);
        } else {
            template\article\renderList($articles);
        }
    }
    endblock();
}
?>
