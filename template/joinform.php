<?php

namespace template\joinform;

?>

<?php function renderJoinForm() { ?>
<h2>Sign Up</h2>
<form id="joinform" action="join.php" method="post" class="pure-form pure-form-stacked">
    <label>ID : <input type="text" class="id" name="id" required="required" pattern="^[\w]{4,20}$" /></label>
    <label>PASSWORD : <input type="password" name="password" required="required" pattern="^.{4,20}$" /></label>
    <label>NAME : <input type="text" name="name" required="required" pattern="^.{1,40}$" /></label>
    <input type="submit" class="pure-button pure-button-primary joinButton" value="join"/>
</form>
<?php } ?>
