<? namespace template\article; ?>

<? function renderListHeader() { ?>
    <h2>Articles <a href="rss.php<?= \http\query_string($_GET, array("month"=>null))?>"><i class = "fa fa-rss-square"></i></a></h2>
<? } ?>

<? function renderList($articles) { ?>
    <p>
    <? if (isset($_SESSION['user'])) { ?>
        Why don't you <a href="article_edit.php">write one</a>?
        <!-- add by 이정훈 ADMIN delete 버튼 활성 -->
    <? } else { ?>
        Want to write a article? Login or <a href="join.php">join</a>.
    <? } ?>
    </p>
    <p>View as <a href="<?= \http\query_string($_GET, array('month'=>date('m')))?>">Calendar</a></p>
    <section id="articles">
    <? if (count($articles) > 0) { ?>
        <form action="article_delete.php" method="POST">
        <? foreach ($articles as $article) { ?>
        <div class="article-entry">
        <? if (isset($_SESSION['user']) and $_SESSION['user']['level'] >= 99) { ?>
            <span><input type="checkbox" name="articles[]" value="<?=$article['id']?>"></span>
        <? } ?>
            <span class="headline"><a href="article.php?id=<?=$article["id"]?>"><?=htmlspecialchars($article["title"]) ?></a></span><span class="content"><?=htmlspecialchars(mb_substr($article["content"], 0, 40, "utf8")) ?></span>
            <span class="user"><?=$article["user_name"]?></span>
            <span class="course"><?=$article["course_name"]?> <span class="category"><?=$article["category_name"]?></span></span>
        </div>
        <? } ?>
        <? if (isset($_SESSION['user']) and $_SESSION['user']['level'] >= 99) { ?>
        <input type="submit" value="delete" class="pure-button pure-button-warning">
        <? } ?>
        </form>
    <? } else { ?>
        <p>no entry</p>
    <? } ?>
    </section>
<? } ?>

<? function renderCalendar($articles, $month) { ?>
    <? function renderMonthLink($month, $thisMonth=false) { ?>
        <?
            if ($thisMonth) {
                $title = "이번 달";
                $link = \http\query_string($_GET, array('month'=>date('m')));
            } else {
                $title = $month;
                $link = \http\query_string($_GET, array('month'=>$month));
            }

            $cls = "";

            if ($month === date('m')) {
                $cls = "pure-button-disabled";
                $link .= "#";
            }
        ?>
        <? if (validMonth($month)) { ?>
        <a href="article.php<?= $link ?>" class="pure-button pure-button-xsmall <?= $cls ?>"><?= $title ?></a>
        <? } ?>
    <? } ?>
    <?
    $year = date('Y');

    if (!validMonth($month)) {
        $month = date('m');
    }


    $time = strtotime($year.'-'.$month.'-01');
    list($tday, $sweek) = explode('-', date('t-w', $time));  // 총 일수, 시작요일
    $tweek = ceil(($tday + $sweek) / 7);  // 총 주차
    $lweek = date('w', strtotime($year.'-'.$month.'-'.$tday));  // 마지막요일

    ?>
    <h3><?=$year?>년 <?=$month?>월</h3>
    <p>
        <? renderMonthLink($month - 1); ?><? renderMonthLink($month, true); ?><? renderMonthLink($month + 1); ?> View as <a href="article.php<?= \http\query_string($_GET, array('month'=>null))?>">List</a>
    </p>
    <section id="calendar">
    <table id="calendar_table">
        <colgroup>
            <col class="weekend" />
            <col />
            <col />
            <col />
            <col />
            <col />
            <col class="weekend" />
        </colgroup>
        <thead><tr><th>일</th><th>월</th><th>화</th><th>수</th><th>목</th><th>금</th><th>토</th></tr></thead>

        <tbody>
        <? for ($n=1,$i=0; $i<$tweek; $i++): ?>
            <tr>
            <? for ($k=0; $k<7; $k++): ?>
                <td>
                    <? if (($i == 0 && $k < $sweek) || ($i == $tweek-1 && $k > $lweek)){ ?>
                    </td>
                    <? continue;} ?>
                    <p><?=$n++?></p>
                    <? foreach ($articles as $article) {
                        $delta_s = strtotime($year."-".$month."-".$n) - strtotime($article['startdate']);
                        $delta_e = strtotime($article['enddate']) - strtotime($year."-".$month."-".$n) + 86401;
                        if ($delta_s>0 and $delta_e > 0 ) { ?>
                    <div><span class="headline"><a href="article.php?id=<?=$article["id"]?>"><?=htmlspecialchars($article["title"]) ?></a></span></div>
                        <? } ?>
                    <? } ?>
                </td>
            <? endfor; ?>
            </tr>
        <? endfor; ?>
        </tbody>
    </table>
    </section>

<? } ?>
