<?php

namespace template\article;

function renderContent($text) {
    return \mb_ereg_replace("\\n", "<br />", htmlspecialchars($text));
}

?>

<? function renderEntry($article) { ?>
    <? global $coursefeed; ?>
    <h2><?= htmlspecialchars($article["title"]) ?></h2>
    <section class="article-entry">
    <p><?= $article["course_name"]?> / <?= $article["category_name"]?></p>
    <p><?= renderContent($article["latest_revision"]["content"]) ?></p>

    <p>publisher : <?= htmlspecialchars($article["user_name"]) ?><br />
        @ <?= $article["latest_revision"]["uploaded_at"] ?></p>
    <p><?=$article["latest_revision"]["startdate"]?> ~ <?=$article["latest_revision"]["enddate"]?></p>
    </section>

    <p class="upvotes">upvotes: <span class="count"><?= $article["upvotes"] ?></span>
        <button class="pure-button" <? if (!$article["can_upvote"]) { ?>disabled="disabled" <? } ?> data-article-id="<?= $article['id'] ?>">
            <i class="fa fa-thumbs-up"></i> <? if (!$article["can_upvote"] && $coursefeed->isLoggedIn()) { ?>upvoted<? } else { ?>upvote<? } ?>
        </button>
    </p>
    <? if (count($article["revisions"]) > 0) { ?>
        <h3>revisions</h3>
        <ol>
        <? foreach ($article["revisions"] as $revision) { ?>
            <li><?= renderContent($revision["content"]) ?><br ?>@ <?= $revision["uploaded_at"] ?></li>
        <? } ?>
        </ol>
    <? } ?>
    <p>
    <? if( isset($_SESSION['user'])) { ?>
        <? if( $_SESSION['user']['user_id'] == $article['user_id']) { ?>
        <a href="article_edit.php?id=<?=$article["id"]?>">edit</a> |
        <? } if( $_SESSION['user']['user_id'] == $article['user_id'] || $_SESSION['user']['level'] >= 99) { ?>
        <a href="article_delete.php?id=<?=$article["id"]?>">delete</a> |
        <? } ?>
    <? } ?>
        <a href="article.php">list</a>
    </p>
<? } ?>

<? function renderNotFound() { ?>
    <h2>Article Not Found</h2>
    <p>Quite embarrasing, isn't it?</p>
<? }?>

<? function renderInsertForm($courses, $categories) { ?>
 <form method="post" class="article pure-form pure-form-stacked">
    <div><label>Title: <input id="title" name="title" /></label></div>
    <div><label>Courses:<select name="course">
        <? foreach ($courses as $course_id) {?>
            <option value="<?=$course_id['id']?>">[<?=$course_id['code']?>] <?= htmlspecialchars($course_id['name'])?> <?=$course_id['year']?></option>
        <? } ?>
    </select></label></div>
    <a href="course_edit.php">Add New course</a>

    <div><label>Categories:<select name="category">
        <? foreach ($categories as $category_id) {?>
            <option value="<?=$category_id['id']?>"><?=htmlspecialchars($category_id['name'])?></option>
        <? } ?>
    </select></label></div>
    <a href="category_edit.php">Add New category</a>
    <!-- 2013.11.15 added by 이정훈 - 글쓰기에 date form 추가. -->
    <div><label>StartDate<input type="date" id="startdate" name="startdate" value="<?=date('Y-m-d')?>"></label></div>
    <div><label>EndDate<input type="date" id="enddate" name="enddate" value="<?=date('Y-m-d')?>"></label></div>

    <div><label>Content: <textarea id="contentarea" name="content" cols="80" rows="5"></textarea></label></div>
    <div><input type="submit" value="write" id="submit" class="pure-button pure-button-primary" /> | <a href="article.php">list</a></div>
</form>
<? } ?>

<? function renderEditForm($article) { ?>
 <form method="post" class="article pure-form pure-form-stacked">
    <div>Title: <?= $article["title"] ?></div>
    <div><label>Content: <textarea name="content" cols="80" rows="5"><?= htmlspecialchars($article["latest_revision"]["content"]) ?></textarea></label></div>
    <input type="hidden" name="id" value="<?= $article["id"] ?>" />
    <!-- 2013.11.15 added by 이정훈 - 글수에 date form 추가. -->
    <div><label>StartDate<input type="date" id="startdate" name="startdate" value="<?= $article["latest_revision"]["startdate"] ?>" /></label></div>
    <div><label>EndDate<input type="date" id="enddate" name="enddate" value="<?= $article["latest_revision"]["enddate"] ?>"></label></div>
    <div><input type="submit" value="edit" id="submit" class="pure-button pure-button-primary" /> | <a href="article.php">list</a></div>
</form>
<? } ?>

<!-- add by 이정훈 delete 확 -->
<? function renderDeleteForm($articles) { ?>
 <form method="post" class="article pure-form pure-form-stacked">
    <h2>Are you sure ?</h2>
    <p>Delete # <?=implode(",", $articles)?></p>
    <? foreach ($articles as $article) { ?>
        <span></span>
        <input type="hidden" name="articles[]" value="<?=$article?>">
    <? } ?>
    <div><input type="submit" value="Delete" name="delete" class="pure-button pure-button-warning" /> <input type="submit" value="Back" name="back" class="pure-button" /></div>
</form>
<? } ?>
