<?php

namespace template\loginform;

?>

<?php function renderLoginForm($name) {
    global $coursefeed;

    if($name == "") { ?>
    <form action="login.php" method="post" class="pure-form pure-form-stacked">
        <label>ID : <input type="text" name="id" /></label>
        <label>PW : <input type="password" name="password" /></label>
        <input type="submit" value="login" class="pure-button" /> or <a href="join.php">Join</a>.
    </form>
    <?php } else {
           ?>
        <p>안녕하세요. <?= $_SESSION['user']['name']?>님</p>
        <form action="logout.php" method="post" class="pure-form pure-form-stacked">
            <input type="submit" value="logout" class="pure-button" />
            <? if ($coursefeed->isAdmin()) { ?>
                <a href="adminuser.php">Manage users</a>
            <? } ?>
        </form>
    <?php } ?>
<?php } ?>
