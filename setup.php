<?php

require_once("db.php");
require_once("coursefeed.php");
require_once("template/article.php");
include('base.php'); // base template

function loadJSON($filename)
{
    return json_decode(file_get_contents($filename), true);
}

function loadFixture($fixture) {
    return loadJSON(sprintf("fixture/%s.json", $fixture));
}

$pdo = db\connect();
$pdo->exec(file_get_contents("sql.sql"));

$cf = new CourseFeed();

foreach (loadFixture("course") as $course) {
    $cf->insertCourse($course["code"], $course["name"], $course["year"]);
}

foreach (loadFixture("category") as $category) {
    $cf->insertCategory($category["name"]);
}

foreach (loadFixture("users") as $user) {
    $level = 1;

    if (array_key_exists("level", $user)) {
        $level = $user["level"];
    }
    $cf->insertUser($user["user_id"], $user["password"], $user["name"], $level);
}

?>

<? startblock('content'); ?>
<section class="banner">
<h2>Setup Complete! Happy coding!</h2>
<p><a href=".">Get Started</a></p>
</section>
<? endblock('content'); ?>
